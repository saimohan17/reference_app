// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

const flights = require('../data/flights.json')

const pilots = require('../data/pilots.json')



const planes = require('../data/planes.json')


// const planes = require('../data/planes.json')
 
module.exports = (app) => {
  LOG.info('START seeder.')
  const db = new Datastore()
  db.loadDatabase()
 
  // insert the sample data into our data store
  // db.insert(puppies)
  // db.insert(flights)
  // db.insert(planes)
  db.insert(pilots)
  db.insert(planes)
  db.insert(flights)



  // initialize app.locals (these objects will be available to our controllers)

  // app.locals.flights = db.find(puppies)
  // app.locals.flights = db.find(flights)
  app.locals.pilots = db.find(pilots)
  // app.locals.planes = db.find(planes)


  // LOG.debug(`${puppies.length} puppies`)
  // LOG.debug(`${flights.length} flights`)
  LOG.debug(`${pilots.length} pilots`)
  // LOG.debug(`${planes.length} planes`)

  
  app.locals.planes = db.find(planes)
  app.locals.flights = db.find(flights)

  LOG.debug(`${planes.length} planes`)
  LOG.debug(`${flights.length} flights`)
  LOG.info('END Seeder. Sample data read and verified.')
}